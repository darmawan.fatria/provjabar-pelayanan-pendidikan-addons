from openerp.osv import fields, osv
import time


class izin_belajar(osv.Model):
    _inherit = 'izin.belajar'
    #Laporan
    def download_sk_izin_belajar_docx(self, cr, uid, ids, context=None):
        obj = self.browse(cr,uid,ids,context)[0]
        value = {'izin_belajar_id': [obj.id, obj.name], 'id': obj.id}

        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'izin.belajar.docx.report',
            'form': value
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'izinbelajar.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }

izin_belajar()

class izin_belajar_docx_report(osv.osv_memory):
    
    _name = "izin.belajar.docx.report"
    _columns = {
        'izin_belajar_id'        : fields.many2one('izin.belajar', 'Kenaikan Jabatan'),

    }
    _defaults = {
        'izin_belajar_id':3
    }

    
    def get_izin_belajar_docx_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        print " value ",value
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'izin.belajar.docx.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'izinbelajar.docx.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
izin_belajar_docx_report()
