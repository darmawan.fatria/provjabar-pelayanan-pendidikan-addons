import time
from openerp.report import report_sxw
#from openerp.addons.df_izin_belajar.konfigurasi_periode_izin_belajar import PERIODE_BULAN_LIST
import locale

class izin_belajar_docx_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(izin_belajar_docx_report_parser, self).__init__(cr, uid, name, context=context)

    def get_izin_belajar_docx_report_raw(self,filters,context=None):

        domain = []
        print filters
        layanan_pool = self.pool.get('izin.belajar')
        if filters['form']['izin_belajar_id']:
            izin_belajar_id = filters['form']['izin_belajar_id'][0]
            domain.append(('id','=',izin_belajar_id))


        result_ids=layanan_pool.search(self.cr, self.uid,domain , context=None)
        results = layanan_pool.browse(self.cr, self.uid, result_ids)

        return results

    def get_hello(self,d,upper=True):
        
        return 'hello from parser'
   
    def get_jabatan_lbl(self,d,upper=True):
        lbl = d and d.jabatan_id and d.jabatan_id.name or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_jenjang_jbt_lbl(self,d,upper=True):
        lbl = d and d.jenjang_jabatan_id and d.jenjang_jabatan_id.name or ''
        if upper:
            return lbl.upper()
        return lbl

    def get_nama_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.employee_id.fullname or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_nip_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.employee_id.nip or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_jabatan_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.job_id and d.job_id.name or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_golongan_pegawai(self,d,upper=False):
        lbl = d and d.employee_id and d.golongan_id and d.golongan_id.description or ''
        if upper:
            return lbl.upper()
        return lbl
    def get_opd_pegawai(self,d,upper=False):
        lbl = d and d.company_id and d.company_id.name or ''
        if upper:
            return lbl.upper()
        return lbl

    def _get_terbilang(self,x):
        satuan=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
        n = int(x)
        if n >= 0 and n <= 11:
            Hasil = " " + satuan[n]
        elif n >= 12 and n <= 19:
            Hasil = self._get_terbilang(n % 10) + " Belas"
        elif n >= 20 and n <= 99:
            Hasil = self._get_terbilang(n / 10) + " Puluh" + self._get_terbilang(n % 10)
        elif n >= 100 and n <= 199:
            Hasil = " Seratus" + self._get_terbilang(n - 100)
        elif n >= 200 and n <= 999:
            Hasil = self._get_terbilang(n / 100) + " Ratus" + self._get_terbilang(n % 100)
        elif n >= 1000 and n <= 1999:
            Hasil = " Seribu" + self._get_terbilang(n - 1000)
        elif n >= 2000 and n <= 999999:
            Hasil = self._get_terbilang(n / 1000) + " Ribu" + self._get_terbilang(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            Hasil = self._get_terbilang(n / 1000000) + " Juta" + self._get_terbilang(n % 1000000)
        else:
            Hasil = self._get_terbilang(n / 1000000000) + " Milyar" + self._get_terbilang(n % 100000000)

        return Hasil
    def get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'

        return formatted_print_date

   
    
