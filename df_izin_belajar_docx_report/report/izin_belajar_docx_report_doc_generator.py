from docx import Document
from docx.shared import Pt
from docx.enum.text import WD_BREAK
import cStringIO
import StringIO
from report_engine_xls import report_xls
from izin_belajar_docx_report_parser import izin_belajar_docx_report_parser

class izin_belajar_docx_report_doc_generator(report_xls):

    def generate_doc_report(self, parser, filters, obj, document):

        print "add title....";
        datas = parser.get_izin_belajar_docx_report_raw(filters)
        for data in datas:
            for paragraph in document.paragraphs:
                if '[P_LAYANAN]' in paragraph.text:
                    paragraph.text = 'IZIN BELAJAR PEGAWAI NEGERI SIPIL DI LINGKUNGAN PEMERINTAH PROVINSI JAWA BARAT';
                if '[P_NAMA_LENGKAP]' in paragraph.text:
                    paragraph.text = parser.get_nama_pegawai(data)
                
            for table in document.tables:
                for row in table.rows:
                    for cell in row.cells:
                        if '[P_NAMA_LENGKAP]' in cell.text:
                            cell.text = cell.text.replace('[P_NAMA_LENGKAP]',parser.get_nama_pegawai(data))
                        if '[P_NIP]' in cell.text:
                            cell.text = cell.text.replace('[P_NIP]',parser.get_nip_pegawai(data))
                        if '[P_JABATAN]' in cell.text:
                            cell.text =  cell.text.replace('[P_JABATAN]',parser.get_jabatan_pegawai(data))
                        if '[P_GOLONGAN]' in cell.text:
                            cell.text =  cell.text.replace('[P_GOLONGAN]',parser.get_golongan_pegawai(data))
                        if '[P_OPD]' in cell.text:
                            cell.text =  cell.text.replace('[P_OPD]', parser.get_opd_pegawai(data))
                        if '[P_NO_SURAT_OPD]' in cell.text:
                            cell.text = cell.text.replace('[P_NO_SURAT_OPD]', data.no_sp_izin_belajar)
                        if '[P_TGL_OPD]' in cell.text:
                            cell.text = cell.text.replace('[P_TGL_OPD]', parser.get_format_date(data.tanggal_surat_permohonan))
                        if '[P_TAHUN_FORMASI]' in cell.text:
                            cell.text = cell.text.replace('[P_TAHUN_FORMASI]',data.tahun_pengajuan or '-')
                        if '[P_NO_AKREDITASI]' in cell.text:
                            cell.text = cell.text.replace('[P_NO_AKREDITASI]', data.no_sk_banpt or '-')
                        if '[P_INDEKS_AKREDITASI]' in cell.text:
                            cell.text = cell.text.replace('[P_INDEKS_AKREDITASI]', data.akreditasi and data.akreditasi.upper() or '-')
                        if '[P_SEKOLAH]' in cell.text:
                            cell.text = cell.text.replace('[P_SEKOLAH]',
                                                          data.nama_sekolah_id and data.nama_sekolah_id.name.upper() or '-')
                        if '[P_NO_SURAT_LULUS]' in cell.text:
                            cell.text = cell.text.replace('[P_NO_SURAT_LULUS]', data.no_sk_lulus or '-')
                        if '[P_TGL_SURAT_LULUS]' in cell.text:
                            cell.text = cell.text.replace('[P_TGL_SURAT_LULUS]',parser.get_format_date(data.tanggal_sk_lulus))

                        if '[P_JENJANG_STUDI]' in cell.text:
                            cell.text = cell.text.replace('[P_JENJANG_STUDI]',
                                                                    data.level_pendidikan_id and data.level_pendidikan_id.name.upper() or '-')
                        if '[P_PROGRAM_STUDI]' in cell.text:
                            cell.text = cell.text.replace('[P_PROGRAM_STUDI]',
                                                                    data.program_studi_id and data.program_studi_id.name.upper() or '-')
                        if '[P_SEKOLAH]' in cell.text:
                            cell.text = cell.text.replace('[P_SEKOLAH]',
                                                                    data.nama_sekolah_id and data.nama_sekolah_id.name.upper() or '-')
                        if '[P_TAHUN_AKADEMIK]' in cell.text:
                            cell.text = cell.text.replace('[P_TAHUN_AKADEMIK]', data.tahun_akademik.upper() or '-')
                        if '[P_LAMA_SEKOLAH]' in cell.text:
                            cell.text = cell.text.replace('[P_LAMA_SEKOLAH]', str(data.masa_perkuliahan) or '-')
                        if '[P_LAMA_SEKOLAH_TERBILANG]' in cell.text:
                            cell.text = cell.text.replace('[P_LAMA_SEKOLAH_TERBILANG]',parser._get_terbilang(str(data.masa_perkuliahan)) or '-')

                        for cell_table in cell.tables:
                            for cell_row in cell_table.rows:
                                for cell_cell in cell_row.cells:
                                    if '[P_OPD]' in cell_cell.text:
                                         cell_cell.text= cell_cell.text.replace('[P_OPD]', parser.get_opd_pegawai(data))
                                    # if '[P_JENJANG_STUDI]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_JENJANG_STUDI]',
                                    #                                             data.level_pendidikan_id and data.level_pendidikan_id.name.upper() or '-')
                                    # if '[P_PROGRAM_STUDI]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_PROGRAM_STUDI]',
                                    #                                             data.program_studi_id and data.program_studi_id.name.upper() or '-')
                                    # if '[P_SEKOLAH]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_SEKOLAH]',
                                    #                                   data.nama_sekolah_id and data.nama_sekolah_id.name.upper() or '-')
                                    # if '[P_TAHUN_AKADEMIK]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_TAHUN_AKADEMIK]',data.tahun_akademik.upper() or '-')
                                    # if '[P_LAMA_SEKOLAH]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_LAMA_SEKOLAH]', data.masa_perkuliahan or '-')
                                    # if '[P_LAMA_SEKOLAH_TERBILANG]' in cell_cell.text:
                                    #     cell_cell.text = cell_cell.text.replace('[P_LAMA_SEKOLAH_TERBILANG]', data.masa_perkuliahan or '-')




    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        #document = Document('/home/darfat/Downloads/template_kenaikan_jabatan_3.docx')


        fpath='/home/skpjabar/odoo/provjabar-pelayanan-pendidikan-addons/df_izin_belajar_docx_report/report/'
        fname='template_izin_belajar.docx';
        f = open(fpath+''+fname)
        document = Document(f)
        style = document.styles['Normal']
        font = style.font
        font.name = 'Bookman Old Style'
        font.size = Pt(12)
        self.generate_doc_report(rml_parser, filters, rml_parser.localcontext['objects'], document)
        f.close()
        document.save(io)
        io.seek(0)
        return (io.read(), 'docx')

#Start the reporting service
izin_belajar_docx_report_doc_generator(
    #name (will be referred from izin_belajar_docx_report.py, must add "report." as prefix)
    'report.izinbelajar.docx.xls',
    #model
    'izin.belajar.docx.report',
    #file
    'addons/df_izin_belajar_docx_report/report/izin_belajar_docx_report.xls',
    #parser
    parser=izin_belajar_docx_report_parser,
    #header
    header=True
)
