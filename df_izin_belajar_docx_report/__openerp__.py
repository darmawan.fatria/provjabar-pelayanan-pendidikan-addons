##############################################################################
#
#    Darmawan Fatriananda
#    BKD Jabar Prov
#    http://mediasee.net
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "SK IZIN BELAJAR DOCX",
    "version": "1.0",
    "author": "Isfan",
    "category": "IB/ Laporan",
    "description": "SK IZIN BELAJAR DOCX",
    "website" : "http//-",
    "license" : "GPL-3",
    "depends": ['df_pendidikan_lanjutan'],
    'data': ["izin_belajar_docx_report.xml",],
    'installable': True,
    'active': False,
}
