import openerp.pooler
import time
from datetime import datetime
import openerp.tools
import logging
from openerp.report import report_sxw
from datetime import datetime
from openerp.addons.report_webkit import webkit_report
from openerp.tools.translate import _
from openerp.osv import osv
import math
import locale

class formasi_izin_belajar_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(formasi_izin_belajar_report_parser, self).__init__(cr, uid, name, context=context)
    
    def get_formasi_izin_belajar_report_raw(self,filters,context=None):
        formasi_pool = self.pool.get('formasi.pendidikan.lanjutan')
        p_tahun_formasi=filters['form']['tahun_pembuatan']
        formasi_izin_belajar_ids= formasi_pool.search(self.cr,self.uid,[('tahun_pembuatan','=',p_tahun_formasi),
                                                                     ('type','=','ib'),
                                                                     ],order="company_id,level_pendidikan_id,program_studi_id asc")
        results = formasi_pool.browse(self.cr, self.uid, formasi_izin_belajar_ids)
        return results;
    def get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
            formatted_print_date = formatted_print_date.replace('Pebruari','Februari')
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def get_title(self,prefiks,filters):
        p_tahun_pengajuan=filters['form']['tahun_pembuatan']
      
        return prefiks+' '+p_tahun_pengajuan
   
    
