from openerp.osv import fields, osv


class formasi_izin_belajar_report(osv.osv_memory):
    
    _name = "formasi.izin.belajar.report"
    _columns = {
        'tahun_pembuatan' :fields.char('Tahun Pembuatan',size=4,required=True),
        'company_id': fields.many2one('res.company', 'OPD',),
        'level_pendidikan_id': fields.many2one('partner.employee.study.degree', 'Jenjang Pendidikan',),
    }
    
    def get_formasi_izin_belajar_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'formasi.izin.belajar.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'formasi.izin.belajar.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
formasi_izin_belajar_report()
