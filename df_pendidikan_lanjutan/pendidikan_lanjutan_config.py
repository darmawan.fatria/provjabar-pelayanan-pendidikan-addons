from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #

class pendidikan_lanjutan_config(osv.Model):
    _name = 'pendidikan.lanjutan.config'
    _description = 'Persyaratan Pelayanan Pendidikan Lanjutan'
    _columns = {
        'name' : fields.char('Nama Persyaratan',size=100,required=True),
        'code' : fields.char('Kode Persyaratan',size=16,required=True),
        'description' : fields.text('Deskripsi',),
        'int_value' : fields.integer('Nilai Integer'),
        'type'     : fields.selection([('ib', 'Izin Belajar'), ('tb', 'Tugas Belajar'),
                                       ], 'Jenis Pelayanan Pendidikan'
                                      ,required=True),
        'active' : fields.boolean('Aktif'),
    }
    _defaults = {
        'active' : True,
     
    }
    
pendidikan_lanjutan_config()


class partner_employee_study_degree(osv.Model):
    _inherit= "partner.employee.study.degree"
    
    _columns = {
    'pangkat_minimum': fields.many2one('partner.employee.golongan', 'Pangkat Minimum',required=True,), 
    'masa_kerja_pangkat' : fields.integer('Masa Kerja Pangkat (Tahun)',help="Pangkat Minimum Selama Berapa Tahun?"),
    'izin_belajar_desc' : fields.text('Deskripsi Persyaratan Izin Belajar',),
    'employee_id': fields.many2one('res.partner', 'Penanggung Jawab Izin Belajar',required=True,), 
    'masa_perkuliahan' : fields.integer('Masa Perkuliahan (Tahun)',),
    #SK field
    'sk_ketentuan' : fields.text('Ketentuan SK',),
    'sk_izin' : fields.text('Dasar SK, Poin D',),
    'sk_keterangan' : fields.text('Dasar SK, Poin E',),
    }
   
partner_employee_study_degree()

class partner_employee_school_location(osv.Model):
    _name = 'partner.employee.school.location'
    _description = 'Lokasi Perguruan Tinggi'
    _columns = {
        'name' : fields.char('Lokasi',size=80,required=True),
        'akreditasi'     : fields.selection([('A', 'A'), 
                                             ('B', 'B'),
                                             ('C', 'C'),
                                            ], 'Minimum Akrediatasi'),
    }
   
    
partner_employee_school_location()


class partner_employee_school(osv.Model):
    _inherit = "partner.employee.school"

    _columns = {
        'izin_belajar_state'     : fields.selection([('available_ib', 'Bisa Untuk Izin Belajar'), 
                                        ('unavailable_ib', 'Tidak Bisa Untuk Izin Belajar')],'Status Izin Belajar'),
        'location': fields.many2one('partner.employee.school.location', 'Lokasi',required=False),
        
    }
    _defaults = {
        'izin_belajar_state' : 'available_ib',
     
    }
partner_employee_school()

class partner_employee_school_accreditation(osv.Model):
    _name = 'partner.employee.school.accreditation'
    _description = 'Akreditasi Program Studi Perguruan Tinggi'
    _columns = {
        'name' : fields.char('No. SK',size=80,required=True),
        'tahun_sk' : fields.char('Tahun SK',size=4),
        'date' : fields.date('Tanggal Kadaluarsa'),
        'date_start' : fields.date('Tanggal Penetapan SK'),
        'state'     : fields.selection([('masih_berlaku', 'Masih Berlaku'), 
                                             ('kadaluarsa', 'Kadaluarsa'),
                                            ], 'Status'),
        'akreditasi'     : fields.selection([('A', 'A'), 
                                             ('B', 'B'),
                                             ('C', 'C'),
                                            ], 'Akreditasi',required=True),
        'level_pendidikan_id': fields.many2one('partner.employee.study.degree', 'Level Jenjang Pendidikan',required=False),
        'tingkat_pendidikan_id': fields.many2one('partner.employee.study.type', 'Tingkat Pendidikan',required=False),
        'program_studi_id': fields.many2one('partner.employee.study', 'Program Studi',required=True),
        'nama_sekolah_id': fields.many2one('partner.employee.school', 'Nama Sekolah',required=True),
        'description' : fields.text('Deskripsi Status Akreditasi'),
       
    }
   
    
partner_employee_school_accreditation()
#-- Temp
class temp_ib_config(osv.Model):
    _name = 'temp.ib.config'
    _description = 'Temp ib config'
    _columns = {
        'name' : fields.char('Nama',size=400,required=False),
        'code' : fields.char('Kode',size=50,required=False),
        'description' : fields.text('Deskripsi',),
        'type'     : fields.selection([('1', 'Jurusan'), ('2', 'Daftar Sekolah')
                                       , ('3', 'Jurusan Akreditasi'),
                                       ], 'Jenis Data'
                                      ,required=True),
    }
    
    
temp_ib_config()

