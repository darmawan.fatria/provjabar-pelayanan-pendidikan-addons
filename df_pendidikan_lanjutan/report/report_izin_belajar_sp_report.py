from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale


class report_izin_belajar_sp_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_izin_belajar_sp_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_data_izin_belajar' : self._get_data_izin_belajar,
            'get_config_label_pergub' : self._get_config_label_pergub,
            'get_config_label_kepgub' : self._get_config_label_kepgub,
            'get_format_date' : self._get_format_date,
            'get_no_sk': self._get_no_sk,
        })
        
    def _get_data_izin_belajar(self,data):
        obj_data=self.pool.get('izin.belajar').browse(self.cr,self.uid,data['izin_belajar_ids'])
        
        
        return obj_data
    def _get_config_label_pergub(self):
        config_pool = self.pool.get('pendidikan.lanjutan.config')
        config_ids = config_pool.search(self.cr,self.uid,[('code','=','rep.pergub.ib'),('active','=','True')],context=None)
        o = config_pool.browse(self.cr,self.uid,config_ids,context=None)[0]
        
        return o and o.description or '-'
    def _get_config_label_kepgub(self):
        config_pool = self.pool.get('pendidikan.lanjutan.config')
        config_ids = config_pool.search(self.cr,self.uid,[('code','=','rep.kepgub.ib'),('active','=','True')],context=None)
        o = config_pool.browse(self.cr,self.uid,config_ids,context=None)[0]
        
        return o and o.description or '-'
    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
            formatted_print_date = formatted_print_date.replace('Pebruari','Februari')
        except :
            formatted_print_date='-'
            
        return formatted_print_date

    def _get_no_sk(self, employee):
        no_sk = '                       '
        try:
            if employee.golongan_id_history:
                no_sk = employee.golongan_id_history[0].name
        except:
            no_sk = '                 ';

        return no_sk
class wrapped_report_izin_belajar_sp(osv.AbstractModel):
    _name = 'report.df_pendidikan_lanjutan.report_izin_belajar_sp_report'
    _inherit = 'report.abstract_report'
    _template = 'df_pendidikan_lanjutan.report_izin_belajar_sp_report'
    _wrapped_report_class = report_izin_belajar_sp_report