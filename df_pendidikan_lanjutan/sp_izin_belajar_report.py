from openerp.osv import osv


class sp_izin_belajar_report(osv.osv_memory):
    _name = "sp.izin.belajar.report"

    #_columns = {
        #'name_pic' : fields.many2one('penandatangan.faktur', 'Penandatangan', required='True'),
    #}
    def print_sp_izin_belajar(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        
        datas = {
             'ids': context.get('active_ids',[]),
             'izin_belajar_ids': context.get('active_ids',[]),
             'model': 'sp.izin.belajar.report',
             'src_model': 'izin.belajar',
             'form': value
                 }
        
        return {
            'type': 'ir.actions.report.xml',
             'report_name': 'sp.izin.belajar.report.form',
             'report_type': 'webkit',
             'datas': datas,
            }
sp_izin_belajar_report